"""Unit tests for utility functions"""

from unittest import TestCase
import pandas as pd


class TestUtilities(TestCase):
    def test__add_dict_item_to_df_row_functionality(self) -> None:
        input_data = {
            "album": {
                "album_type": "ALBUM",
                "artists": [
                    {
                        "external_urls": {
                            "spotify": "https://open.spotify.com/artist/6TIYQ3jFPwQSRmorSezPxX"
                        },
                        "href": "https://api.spotify.com/v1/artists/6TIYQ3jFPwQSRmorSezPxX",
                        "id": "6TIYQ3jFPwQSRmorSezPxX",
                        "name": "Machine Gun Kelly",
                        "type": "artist",
                        "uri": "spotify:artist:6TIYQ3jFPwQSRmorSezPxX",
                    }
                ],
            }
        }
        expected_data = {
            "album_type": "ALBUM",
            "artists_0_external_urls_spotify": "https://open.spotify.com/artist/6TIYQ3jFPwQSRmorSezPxX",
            "artists_0_href": "https://api.spotify.com/v1/artists/6TIYQ3jFPwQSRmorSezPxX",
            "artists_0_id": "6TIYQ3jFPwQSRmorSezPxX",
            "artists_0_name": "Machine Gun Kelly",
            "artists_0_type": "artist",
            "artists_0_uri": "spotify:artist:6TIYQ3jFPwQSRmorSezPxX",
        }
        print(expected_data)
        expected_df = pd.DataFrame(expected_data)
        print(expected_df)
