"""This is the backend server to communicate with the spotify API"""

import base64

import requests
from flask import Flask, jsonify, make_response, redirect, request, Response
from constants import CLIENT_ID, CLIENT_SECRET, FLASK_SOCKET, VALID_TIME_RANGES
from utilities import (
    convert_artist_info_to_dataframe,
    convert_track_info_to_dataframe,
    get_top_elements_from_dataframe,
)

app = Flask(__name__)
ACCESS_TOKEN = None

#######################
# Backend -> Frontend #
#######################


def make_CORS_response(message: dict) -> Response:
    """Converts a string to a response objects with valid CORS policy.

    Args:
        message (dict): Message to jsonify and convert to Request.

    Returns:
        Response: Response with headers for CORS.
    """
    resp = make_response(jsonify(message))
    resp.headers["Access-Control-Allow-Origin"] = "*"
    return resp


@app.route("/status")
def status():
    return make_CORS_response(
        {"status": "server up", "message": "I hope this message finds you well."}
    )


@app.route("/testdata")
def send_test_data_json():
    return make_CORS_response(
        {
            "artist_name": [
                "Linkin Park",
                "mgk",
                "Chase & Status",
                "Mike Shinoda",
                "Bring Me The Horizon",
                "Bad Bunny",
                "Die Antwoord",
                "Rise Against",
                "Limp Bizkit",
                "Disarstar",
            ],
            "genre_0": [
                "alternative metal",
                "ohio hip hop",
                "dancefloor dnb",
                "alternative hip hop",
                "melodic metalcore",
                "reggaeton",
                "african rock",
                "alternative metal",
                "alternative metal",
                "german alternative rap",
            ],
        }
    )


@app.route("/get_top_artists")
def get_top_artists():
    if ACCESS_TOKEN is None:
        return make_CORS_response(
            {
                "error": "401 Unauthorized",
                "message": "API authentification was not successful. ACCESS_TOKEN is not set properly.",
            }
        )
    time_range = request.args.get("time_range")
    if time_range is not None and time_range not in VALID_TIME_RANGES:
        return make_CORS_response(
            {
                "error": "400 Bad request",
                "message": f"Invalid time range provided. {VALID_TIME_RANGES=}. Got {time_range}",
            }
        )
    return make_CORS_response(
        get_top_elements_from_dataframe(
            convert_artist_info_to_dataframe(
                requests.get(
                    url=(
                        "https://api.spotify.com/v1/me/top/artists"
                        if time_range is None
                        else f"https://api.spotify.com/v1/me/top/artists?time_range={time_range.lower()}"
                    ),
                    headers={"Authorization": f"Bearer {ACCESS_TOKEN}"},
                ).json()
            ),
            ["artist_name", "genre_0"],
            10,
        )
    )


@app.route("/get_top_tracks")
def get_top_tracks():
    if ACCESS_TOKEN is None:
        return make_CORS_response(
            {
                "error": "401 Unauthorized",
                "message": "API authentification was not successful. ACCESS_TOKEN is not set properly.",
            }
        )
    time_range = request.args.get("time_range")
    if time_range is not None and time_range not in VALID_TIME_RANGES:
        return make_CORS_response(
            {
                "error": "400 Bad request",
                "message": f"Invalid time range provided. {VALID_TIME_RANGES=}. Got {time_range}",
            }
        )
    return make_CORS_response(
        get_top_elements_from_dataframe(
            convert_track_info_to_dataframe(
                requests.get(
                    url=(
                        "https://api.spotify.com/v1/me/top/tracks"
                        if time_range is None
                        else f"https://api.spotify.com/v1/me/top/tracks?time_range={time_range.lower()}"
                    ),
                    headers={"Authorization": f"Bearer {ACCESS_TOKEN}"},
                ).json()
            ),
            ["track_name", "artist_name", "album_name", "album_release_date"],
            10,
        )
    )


##########################
# Backend -> Spotify API #
##########################


@app.route("/callback")
def callback():
    code = request.args.get("code", "")
    # TODO do something if the user rejects the request

    auth_bytes = f"{CLIENT_ID}:{CLIENT_SECRET}".encode("utf-8")
    auth_encoded = str(base64.b64encode(auth_bytes), "utf-8")
    response = requests.post(
        url="https://accounts.spotify.com/api/token",
        data={
            "code": code,
            "redirect_uri": FLASK_SOCKET + "/callback",
            "grant_type": "authorization_code",
            "client_id": CLIENT_ID,
            "client_secret": CLIENT_SECRET,
        },
        headers={
            "Authorization": f"Basic {auth_encoded}",
            "Content-Type": "application/x-www-form-urlencoded",
        },
    )
    access_token = response.json()["access_token"]
    global ACCESS_TOKEN
    ACCESS_TOKEN = access_token
    # TODO add redirect to a page that actually exists
    return redirect("/")


@app.route("/get_token")
def get_token():
    token = request.args.get("token", "")
    return token


@app.route("/login")
def login():
    return redirect(
        "https://accounts.spotify.com/authorize?"
        f"client_id={CLIENT_ID}&redirect_uri={FLASK_SOCKET}/callback&response_type=code&scope=user-top-read"
    )


@app.route("/refresh_token")
def refresh_token():
    # TODO implement functionality
    # https://developer.spotify.com/documentation/web-api/tutorials/refreshing-tokens
    pass


if __name__ == "__main__":
    app.run()
