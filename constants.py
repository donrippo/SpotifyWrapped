"""This file collects global constants used in other files."""

import os

FLASK_SOCKET = "http://127.0.0.1:5000"
CLIENT_ID = os.getenv("SPOTIFY_API_CLIENT_ID")
CLIENT_SECRET = os.getenv("SPOTIFY_API_CLIENT_SECRET")
VALID_TIME_RANGES = ["long_term", "medium_term", "short_term"]
