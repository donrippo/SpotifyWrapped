"""This file contains utilities such as json parsers"""

import pandas as pd


def convert_track_info_to_dataframe(json_data: str) -> pd.DataFrame:
    """Reads a file at the specified location and returns dataframes for track information.

    Args:
        json_data (str): Data string containing a parsed json dictionary.

    Returns:
        pd.DataFrame: Dataframe with selceted and strcutured data from the json string.
    """
    track_df_data = {}
    track_dicts = json_data["items"]
    for idx, track_dict in enumerate(track_dicts):
        track_df_data[idx] = {
            f"track_{key}": value
            for key, value in track_dict.items()
            if not isinstance(value, list) and not (isinstance(value, dict))
        }
        track_df_data[idx].update(
            {
                f"album_{key}": value
                for key, value in track_dict["album"].items()
                if not isinstance(value, list) and not (isinstance(value, dict))
            }
        )
        track_df_data[idx].update(
            {
                f"artist_{key}": value
                for key, value in track_dict["artists"][0].items()
                if not isinstance(value, list) and not (isinstance(value, dict))
            }
        )

    return pd.DataFrame.from_dict(track_df_data).transpose()


def convert_artist_info_to_dataframe(json_data: str) -> pd.DataFrame:
    """Reads a file at the specified location and returns dataframes for artist information.

    Args:
        json_data (str): Data string containing a parsed json dictionary.

    Returns:
        pd.DataFrame: Dataframe with selceted and strcutured data from the json string.
    """
    artist_df_data = {}
    artist_dicts = json_data["items"]
    for idx, artist_dict in enumerate(artist_dicts):
        artist_df_data[idx] = {
            f"artist_{key}": value
            for key, value in artist_dict.items()
            if not isinstance(value, list) and not (isinstance(value, dict))
        }
        artist_df_data[idx].update(
            {
                f"genre_{idx}": value
                for idx, value in enumerate(artist_dict["genres"][:3])
            }
        )
    return pd.DataFrame.from_dict(artist_df_data).transpose()


def get_top_elements_from_dataframe(df: pd.DataFrame, keys: list[str], n: int):
    return df[keys].head(n).to_dict(orient="list")
