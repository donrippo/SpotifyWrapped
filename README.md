## What is this
- This is a server that communicates with the Spofity API to fetch data, prune it and send it to a nextjs server.

## TODO's
- Handle errors if the user denies access to spotify
- Improve the design (e.g. include tailwind or similar)
- Include an option fto store and provide data over time (probably mongoDB or similar)
- Redirect to a page that exists after authentication
- Implement a function to refresh the access token after it expired